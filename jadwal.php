<?php
    include 'conn.php';
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Sistem Penjadwalan Dosen</title>
  </head>
  <body>
  <header>
        <div>
            <ul>
                <li><a href="index.php">DOSEN</a></li>
                <li><a href="kelas.php">KELAS</a></li>
                <li><a href="jadwal.php">JADWAL</a></li>
            </ul>
        </div>
    </header>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="container">
            <div class="row justify-content-center">
                <div class="col-7 ">
                <br>
                <h1 align= "center">JADWAL</h1>
                <br>
                <!-- Start Card Form -->
                    <div class="card">
                            <div class="card-header">Input Jadwal</div>
                    <div class="card-body">
                        <form action="tambah_jadwal.php" method="POST">
                            <div class="mb-3">
                                <label for="id_dosen" class="form-label">ID Dosen</label>
                                <input type="text" class="form-control" name="id_dosen" id="id_dosen" required="" placeholder="Masukkan ID Dosen">
                            </div>
                            <div class="mb-3">
                                <label for="id_kelas" class="form-label">ID Kelas</label>
                                <input type="text" class="form-control" name="id_kelas" id="id_kelas" required="" placeholder="Masukkan ID Kelas">
                            </div>
                            <div class="mb-3">
                                <label for="jadwal" class="form-label">Jadwal (yyyy/mm/dd)</label>
                                <input type="text" class="form-control" name="jadwal" id="jadwal" required="" placeholder="Masukkan Jadwal">
                            </div>
                            <div class="mb-3">
                                <label for="matakuliah" class="form-label">Mata Kuliah</label>
                                <input type="text" class="form-control" name="matakuliah" id="matakuliah" required="" placeholder="Masukkan Mata Kuliah">
                            </div>
                            <br>
                            <center>
                                <button type="submit" name="submit" value="submit" class="btn btn-primary" style= "background-color:#003152;">Submit</button> 
                            </center> 
                        </form>
                        </div>
                        </div>
                        <!-- End Card Form -->

                        <br>
                        <br>

                        <!-- Start Card Table -->
                    <div class="card">
                        <div class="card-header">Daftar Jadwal</div>
                    <div class="card-body">
                        <table class="table">
                            <thead class="table-light">
                            <tr>
                                <th>Nomor</th>
                                <th>Id Jadwal</th>
                                <th>Id Dosen</th>
                                <th>Id Kelas</th>
                                <th>Jadwal</th>
                                <th>Mata Kuliah</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>

                            <tbody>
                            <!-- READ DATA-->
                            <?php
                                $sql = "SELECT * FROM jadwal_kelas ORDER BY id_jadwal ASC";
                                $result = mysqli_query($conn, $sql);

                                if(!$result){
                                    die("Query Error :". mysqli_error($conn). " - ".mysqli_error($conn));
                                }
                                $no=1;
                                while($data = mysqli_fetch_assoc($result)){
                            ?>
                            <tr>
                                <td><?php echo $no;?></td>
                                <td><?php echo $data['id_jadwal'];?></td>
                                <td><?php echo $data['id_dosen'];?></td>
                                <td><?php echo $data['id_kelas'];?></td>
                                <td><?php echo $data['jadwal'];?></td>
                                <td><?php echo $data['matakuliah']?></td>
                                <td>
                                    <a href="edit_jadwal.php?id=<?php echo $data['id_jadwal']?>" class="btn btn-warning"> Edit </a>
                                    <a href="hapus_jadwal.php?id=<?php echo $data['id_jadwal']?>" class="btn btn-danger"> Delete </a>
                                </td>
                            </tr>
                            <?php
                                $no++;
                            }
                            ?>
                            <tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </dv>
    </div>
  </body>
</html>