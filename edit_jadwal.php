<?php
    include 'conn.php';

    if(isset($_GET['id'])){
        $id=($_GET['id']);
        $query= "SELECT * FROM jadwal_kelas WHERE id_jadwal='$id'";
        $result = mysqli_query($conn, $query);
       
        if   (!$result){
            die("Query Error :". mysqli_error($conn)." - ".mysqli_error($conn));
        }
        $data= mysqli_fetch_assoc($result);
        if (!count($data)) {
            echo "<script>alert('Data tidak ditemukan pada database');window.location='jadwal.php';</script>";
         }
    } else {
        echo "<script>alert('Masukkan data ID');window.location='jadwal.php';</script>"; 
    }
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Sistem Penjadwalan Dosen</title>
  </head>
  <body>
  <header>
        <div>
            <ul>
                <li><a href="index.php">DOSEN</a></li>
                <li><a href="kelas.php">KELAS</a></li>
                <li><a href="jadwal.php">JADWAL</a></li>
            </ul>
        </div>
    </header>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="container">
            <div class="row justify-content-center">
                <div class="col-7 ">
                <br>
                <h1 align= "center">EDIT JADWAL KELAS</h1>
                <br>
                <!-- Start Card Form -->
                    <div class="card">
                            <div class="card-header">Edit Data Kelas</div>
                    <div class="card-body">
                        <form action="proses_edit_jadwal.php" method="POST">
                        <input name="id" value="<?php echo $data['id'];?>" hidden/>
                            <div class="mb-3">
                                <label for="id_dosen" class="form-label">ID Dosen</label>
                                <input type="text" class="form-control" name="id_dosen" value="<?php echo $data['id_dosen'];?>" id="id_dosen" required="" placeholder="Masukkan ID Dosen...">
                            </div>
                            <div class="mb-3">
                                <label for="id_kelas" class="form-label">ID Kelas</label>
                                <input type="text" class="form-control" name="id_kelas" value="<?php echo $data['id_kelas'];?>" id="id_kelas" required="" placeholder="Masukkan ID Kelas...">
                            </div>
                            <div class="mb-3">
                                <label for="jadwal" class="form-label">Jadwal (yyyy/mm/dd)</label>
                                <input type="text" class="form-control" name="jadwal" value="<?php echo $data['jadwal'];?>" id="jadwal" required="" placeholder="Masukkan Jadwal...">
                            </div>
                            <div class="mb-3">
                                <label for="matakuliah" class="form-label">Mata Kuliah</label>
                                <input type="text" class="form-control" name="matakuliah" value="<?php echo $data['matakuliah'];?>" id="matakuliah" required="" placeholder="Masukkan Mata Kuliah...">
                            </div>
                            <br>
                            <center>
                                <button type="submit" name="submit" value="submit" class="btn btn-primary" style= "background-color:#003152;">Submit</button> 
                            </center> 
                        </form>
                        </div>
                        </div>
                    </div>
             </div>
             </div>
             </body>
             </html>
