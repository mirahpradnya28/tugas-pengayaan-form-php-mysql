<?php
    include 'conn.php';

    if(isset($_GET['id'])){
        $id=($_GET['id']);
        $query= "SELECT * FROM kelas WHERE id_kelas='$id'";
        $result = mysqli_query($conn, $query);
       
        if   (!$result){
            die("Query Error :". mysqli_error($conn)." - ".mysqli_error($conn));
        }
        $data= mysqli_fetch_assoc($result);
        if (!count($data)) {
            echo "<script>alert('Data tidak ditemukan pada database');window.location='kelas.php';</script>";
         }
    } else {
        echo "<script>alert('Masukkan data ID');window.location='kelas.php';</script>"; 
    }
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Sistem Penjadwalan Dosen</title>
  </head>
  <body>
  <header>
        <div>
            <ul>
                <li><a href="index.php">DOSEN</a></li>
                <li><a href="kelas.php">KELAS</a></li>
                <li><a href="jadwal.php">JADWAL</a></li>
            </ul>
        </div>
    </header>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="container">
            <div class="row justify-content-center">
                <div class="col-7 ">
                <br>
                <h1 align= "center">EDIT DATA KELAS</h1>
                <br>
                <!-- Start Card Form -->
                    <div class="card">
                            <div class="card-header">Edit Data Kelas</div>
                    <div class="card-body">
                        <form action="proses_edit_kelas.php" method="POST">
                        <input name="id" value="<?php echo $data['id'];?>" hidden/>
                            <div class="mb-3">
                                <label for="namakelas" class="form-label">Nama Kelas</label>
                                <input type="text" class="form-control" name="namakelas" value="<?php echo $data['nama_kelas'];?>" id="namakelas" required="" placeholder="Masukkan Nama Kelas...">
                            </div>
                            <div class="mb-3">
                                <label for="prodi" class="form-label">Program Studi</label>
                                <input type="text" class="form-control" name="prodi" value="<?php echo $data['prodi'];?>" id="prodi" required="" placeholder="Masukkan Prodi...">
                            </div>

                            <div class="mb-3">
                                <label for="fakultas_kelas" class="form-label">Fakultas</label>
                                <input type="text" class="form-control" name="fakultas" value="<?php echo $data['fakultas'];?>" id="fakultas" required="" placeholder="Masukkan Fakultas...">
                            </div>
                            <br>
                            <center>
                                <button type="submit" name="submit" value="submit" class="btn btn-primary" style= "background-color:#003152;">Submit</button> 
                            </center> 
                        </form>
                        </div>
                        </div>
                    </div>
             </div>
             </div>
             </body>
             </html>
