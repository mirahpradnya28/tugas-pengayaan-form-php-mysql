<?php
    include('conn.php');
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Sistem Penjadwalan Dosen</title>
  </head>
  <body>
  <header>
        <div>
            <ul>
                <li><a href="index.php">DOSEN</a></li>
                <li><a href="kelas.php">KELAS</a></li>
                <li><a href="jadwal.php">JADWAL</a></li>
            </ul>
        </div>
    </header>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="container">
            <div class="row justify-content-center">
                <div class="col-7 ">
                <br>
                <h1 align= "center">DATA DOSEN</h1>
                <br>
                <!-- Start card form -->
                    <div class="card">
                        <div class="card-header">Input Data Dosen</div>
                    <div class="card-body">
                        <form action="tambah_dosen.php" method="POST" enctype="multipart/form-data">
                            <div class="mb-3">
                                <label for="nip" class="form-label">NIP Dosen</label>
                                <input type="text" class="form-control" name="nip" id="nip" placeholder="Masukkan NIP Dosen...">
                            </div>
                            <div class="mb-3">
                                <label for="nama" class="form-label">Nama Dosen</label>
                                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Dosen...">
                            </div>
                            <div class="mb-3">
                                <label for="prodi" class="form-label">Program Studi</label>
                                <select class="form-select" name="prodi" id="prodi" aria-label="Default select example" >
                                    <option selected>Pilih Program Studi</option>
                                    <option value="S1 Pendidikan Teknik Informatika">S1 Pendidikan Teknik Informatika</option>
                                    <option value="S1 Sistem Informasi">S1 Sistem Informasi</option>
                                    <option value="S1 Ilmu Komputer">S1 Ilmu Komputer</option>
                                    <option value="D3 Manajemen Informatika">D3 Manajemen Informatika</option>
                                    <option value="S1 PGSD">S1 PGSD</option>
                                    <option value="S1 Ilmu Hukum">S1 Ilmu Hukum</option>
                                    <option value="S1 Desain Komunikasi Visual">S1 Desain Komunikasi Visual</option>
                                    <option value="S1 Pendidikan Sastra Inggris">S1 Pendidikan Sastra Inggris</option>
                                    <option value="S1 Pendidikan Matematika">S1 Pendidikan Matematika</option>
                                    <option value="S1 Akuntansi">S1 Akuntansi</option>
                                    <option value="D3 Kebidanan">D3 Kebidanan</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="fakultas" class="form-label">Fakultas</label>
                                <select class="form-select" name="fakultas" id="fakultas" aria-label="Default select example" >
                                    <option selected>Pilih Fakultas</option>
                                    <option value="Fakultas Teknik dan Kejuruan">Fakultas Teknik dan Kejuruan</option>
                                    <option value="Fakultas Bahasa dan Seni">Fakultas Bahasa dan Seni</option>
                                    <option value="Fakultas Olahraga dan Kesehatan">Fakultas Olahraga dan Kesehatan</option>
                                    <option value="Fakultas Kedokteran">Fakultas Kedokteran</option>
                                    <option value="Fakultas Ilmu Pendidikan">Fakultas Ilmu Pendidikan</option>
                                    <option value="Fakultas Ekonomi">Fakultas Ekonomi</option>
                                    <option value="Fakultas Matematika dan Ilmu Pengetahuan Alam">Fakultas Matematika dan Ilmu Pengetahuan Alam</option>
                                    <option value="Fakultas Hukum dan Ilmu Sosial">Fakultas Hukum dan Ilmu Sosial</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="file" class="form-label">Masukkan Foto Dosen</label>
                                <input type="file" name="file" class="form-control" >
                            </div>
                            <br>
                            <center>
                                <button type="submit" name="submit" value="submit" class="btn btn-primary" style= "background-color:#003152;">Submit</button> 
                            </center> 
                        </form>
                    </div>
                    </div>
                    <!-- End Card Form -->

                    <br>
                    <br>

                    <!-- Start Card Table -->
                    <div class="card">
                        <div class="card-header">Daftar Dosen</div>
                    <div class="card-body">
                        <table class="table">
                            <thead class="table-light">
                            <tr>
                                <th>Nomor</th>
                                <th>ID Dosen</th>
                                <th>NIP Dosen</th>
                                <th>Nama Dosen</th>
                                <th>Program Studi</th>
                                <th>Fakultas</th>
                                <th>Foto Dosen</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php
                                $sql = "SELECT * FROM dosen ORDER BY id_dosen ASC";
                                $result = mysqli_query($conn, $sql);

                                if(!$result){
                                    die("Query Error :". mysqli_error($conn). " - ".mysqli_error($conn));
                                }
                                $no=1;
                                while($data = mysqli_fetch_assoc($result)){
                            ?>
                            <tr>
                                <td><?php echo $no;?></td>
                                <td><?php echo $data['id_dosen'];?></td>
                                <td> <?php echo $data['nip_dosen'];?></td>
                                <td> <?php echo $data['nama_dosen'];?></td>
                                <td> <?php echo $data['prodi'];?></td>
                                <td> <?php echo $data['fakultas'];?> </td>
                                <td style="text-align:center;"><img src="gambar/<?php echo $data['foto_dosen'];?>" style="width: 120px;"></td>
                                <td>
                                    <a href="editdosen.php?id=<?php echo $data['id_dosen']?>" class="btn btn-warning"> Edit </a>
                                    <a href="hapus_dosen.php?id=<?php echo $data['id_dosen']?>" class="btn btn-danger"> Delete </a>
                                </td>
                            </tr>
                            <?php
                                $no++;
                            }
                            ?>
                            <tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
    </div>

  </body>
</html>