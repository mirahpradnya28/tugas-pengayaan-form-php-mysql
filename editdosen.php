<?php
    include 'conn.php';

    if(isset($_GET['id'])){
        $id=($_GET['id']);
        $query= "SELECT * FROM dosen WHERE id_dosen='$id'";
        $result = mysqli_query($conn, $query);
       
        if   (!$result){
            die("Query Error :". mysqli_error($conn)." - ".mysqli_error($conn));
        }
        $data= mysqli_fetch_assoc($result);
        if (!count($data)) {
            echo "<script>alert('Data tidak ditemukan pada database');window.location='dosen.php';</script>";
         }
    } else {
        echo "<script>alert('Masukkan data ID');window.location='dosen.php';</script>"; 
    }
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Sistem Penjadwalan Dosen</title>
  </head>
  <body>
  <header>
        <div>
            <ul>
                <li><a href="index.php">DOSEN</a></li>
                <li><a href="kelas.php">KELAS</a></li>
                <li><a href="jadwal.php">JADWAL</a></li>
            </ul>
        </div>
    </header>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="container">
            <div class="row justify-content-center">
                <div class="col-7 ">
                <br>
                <h1 align= "center">EDIT DATA DOSEN</h1>
                <br>
                <!-- Start card form -->
                    <div class="card">
                        <div class="card-header">Input Data Dosen</div>
                    <div class="card-body">
                        <form action="proses_edit_dosen.php" method="POST" enctype="multipart/form-data">
                            <div class="mb-3">
                                <label for="nip" class="form-label">NIP Dosen</label>
                                <input type="text" class="form-control" name="nip" value="<?php echo $data['nip_dosen']; ?>" id="nip" placeholder="Masukkan NIP Dosen...">
                            </div>
                            <div class="mb-3">
                                <label for="nama" class="form-label">Nama Dosen</label>
                                <input type="text" class="form-control" name="nama" value="<?php echo $data['nama_dosen']; ?>"id="nama" placeholder="Masukkan Nama Dosen...">
                            </div>
                            <div class="mb-3">
                                <label for="prodi" class="form-label">Program Studi</label>
                                <input type="text" class="form-control" name="prodi" value="<?php echo $data['prodi'];?>" id="prodi" required="" placeholder="Masukkan Prodi...">
                                
                            </div>
                            <div class="mb-3">
                                <label for="fakultas" class="form-label">Fakultas</label>
                                <input type="text" class="form-control" name="fakultas" value="<?php echo $data['fakultas'];?>" id="fakultas" required="" placeholder="Masukkan Fakultas...">
                               
                            </div>
                            <div class="mb-3">
                                <label for="file" class="form-label">Masukkan Foto Dosen</label>
                                <img src="gambar/<?php echo $data['foto_dosen']; ?>" style="width: 120px;float: left;margin-bottom: 10px;">
                                <input type="file" name="file" class="form-control" >
                            </div>
                            <br>
                            <center>
                                <button type="submit" name="submit" value="submit" class="btn btn-primary" style= "background-color:#003152;">UPDATE</button> 
                            </center> 
                        </form>
                    </div>
                    </div>
                    <!-- End Card Form -->
    </body>
</html>